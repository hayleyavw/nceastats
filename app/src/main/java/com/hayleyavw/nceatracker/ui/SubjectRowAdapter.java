package com.hayleyavw.nceatracker.ui;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.hayleyavw.nceatracker.R;
import com.hayleyavw.nceatracker.model.Subject;

public class SubjectRowAdapter extends ArrayAdapter<Subject> {

    private final Context context;
    private final ArrayList<Subject> itemsArrayList;

    public SubjectRowAdapter(Context context, ArrayList<Subject> itemsArrayList) {

        super(context, R.layout.subject_row_layout, itemsArrayList);

        this.context = context;
        this.itemsArrayList = itemsArrayList;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // 1. Create inflater
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // 2. Get rowView from inflater
        View rowView = inflater.inflate(R.layout.subject_row_layout, parent, false);

        // 3. Get the two text view from the rowView
        TextView labelView = (TextView) rowView.findViewById(R.id.subject_name);
        TextView valueView = (TextView) rowView.findViewById(R.id.subject_level);

        // 4. Set the text for textView
        labelView.setText(itemsArrayList.get(position).getSubjectName());
        valueView.setText("Level " + itemsArrayList.get(position).getSubjectLevel() + " ");

        // 5. return rowView
        return rowView;
    }

}