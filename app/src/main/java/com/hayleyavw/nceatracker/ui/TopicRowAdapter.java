package com.hayleyavw.nceatracker.ui;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.hayleyavw.nceatracker.R;
import com.hayleyavw.nceatracker.model.Topic;

public class TopicRowAdapter extends ArrayAdapter<Topic> {

    private final Context context;
    private final ArrayList<Topic> itemsArrayList;

    public TopicRowAdapter(Context context, ArrayList<Topic> itemsArrayList) {

        super(context, R.layout.topic_row_layout, itemsArrayList);

        this.context = context;
        this.itemsArrayList = itemsArrayList;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // 1. Create inflater
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // 2. Get rowView from inflater
        View rowView = inflater.inflate(R.layout.topic_row_layout, parent, false);

        // 3. Get the two text view from the rowView
        TextView labelView = (TextView) rowView.findViewById(R.id.topic_title);
        TextView valueView = (TextView) rowView.findViewById(R.id.topic_credits);
        TextView gradeView = (TextView) rowView.findViewById(R.id.topic_grade);
        TextView intExtView = (TextView) rowView.findViewById(R.id.topic_int_ext);
        TextView pracFinView = (TextView) rowView.findViewById(R.id.topic_prac_fin);

        // 4. Set the text for textView
        labelView.setText(itemsArrayList.get(position).getTopicTitle());
        int num = itemsArrayList.get(position).getNumCredits();
        String numCredits = Integer.toString(num);
        valueView.setText(numCredits);
        gradeView.setText(itemsArrayList.get(position).getGrade());
        intExtView.setText(itemsArrayList.get(position).getIntExtType());
        pracFinView.setText(itemsArrayList.get(position).getPracFinStatus());

        // 5. return rowView
        return rowView;
    }
}