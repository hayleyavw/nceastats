package com.hayleyavw.nceatracker.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.hayleyavw.nceatracker.R;
import com.hayleyavw.nceatracker.model.Year;

import java.util.ArrayList;

public class AddYearRowAdapter extends ArrayAdapter<Year> {

    private final Context context;
    private final ArrayList<Year> itemsArrayList;

    public AddYearRowAdapter(Context context, ArrayList<Year> itemsArrayList) {

        super(context, R.layout.year_row_layout, itemsArrayList);

        this.context = context;
        this.itemsArrayList = itemsArrayList;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // 1. Create inflater
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // 2. Get rowView from inflater
        View rowView = inflater.inflate(R.layout.add_year_row_layout, parent, false);

        // 3. Get the two text view from the rowView
        TextView labelView = (TextView) rowView.findViewById(R.id.date);

        // 4. Set the text for textView
        labelView.setText(itemsArrayList.get(position).getDate());

        // 5. retrn rowView
        return rowView;
    }
}