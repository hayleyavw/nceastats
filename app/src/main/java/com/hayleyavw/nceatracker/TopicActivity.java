package com.hayleyavw.nceatracker;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.*;
import com.hayleyavw.nceatracker.db.TopicsDataSource;
import com.hayleyavw.nceatracker.model.Topic;
import com.hayleyavw.nceatracker.ui.TopicRowAdapter;

import java.util.ArrayList;


public class TopicActivity extends Activity {

    private TopicsDataSource datasource;

    ListView topicList;
    long subjectID;
    long yearID;


    @Override
    public void onCreate(Bundle savedInstanceState) {

        subjectID = getIntent().getLongExtra("EXTRA_SUBJECT_ID", -1);
        yearID = getIntent().getLongExtra("EXTRA_YEAR_ID", -1);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.topic_list_layout);

        datasource = new TopicsDataSource(this);
        datasource.open();

        ArrayList<Topic> values = datasource.getTopicsBySubject(subjectID);

        topicList = (ListView) findViewById(R.id.topicList);

        TopicRowAdapter topicAdapter = new TopicRowAdapter(this, values);

        topicList.setAdapter(topicAdapter);

        TextView subjectNameView = (TextView) findViewById(R.id.subject_name);
        subjectNameView.setText(datasource.getSubjectNameTitle(subjectID));

        int notAchievedCredits = datasource.getAllGradeCreditsBySubject(subjectID, "'Not Achieved'");
        int achievedCredits = datasource.getAllGradeCreditsBySubject(subjectID, "'Achieved'");
        int meritCredits = datasource.getAllGradeCreditsBySubject(subjectID, "'Merit'");
        int excellenceCredits = datasource.getAllGradeCreditsBySubject(subjectID, "'Excellence'");
        int totalCredits = notAchievedCredits + achievedCredits + meritCredits + excellenceCredits;

        TextView creditTotalView = (TextView) findViewById(R.id.credit_total);
        creditTotalView.setText("Total Credits: " + Integer.toString(totalCredits));

        TextView notAchievedCreditTotalView = (TextView) findViewById(R.id.not_achieved_credit_total);
        notAchievedCreditTotalView.setText(Integer.toString(notAchievedCredits) + "NA");
        notAchievedCreditTotalView.setTextColor(Color.rgb(64, 136, 207));

        TextView achievedCreditTotalView = (TextView) findViewById(R.id.achieved_credit_total);
        achievedCreditTotalView.setText(Integer.toString(achievedCredits) + "A");
        achievedCreditTotalView.setTextColor(Color.rgb(22, 116, 206));

        TextView meritCreditTotalView = (TextView) findViewById(R.id.merit_credit_total);
        meritCreditTotalView.setText(Integer.toString(meritCredits) + "M");
        meritCreditTotalView.setTextColor(Color.rgb(5, 73, 139));

        TextView excellenceCreditTotalView = (TextView) findViewById(R.id.excellence_credit_total);
        excellenceCreditTotalView.setText(Integer.toString(excellenceCredits) + "E");
        excellenceCreditTotalView.setTextColor(Color.rgb(3, 57, 108));

        //progress bar

        final float scale = this.getResources().getDisplayMetrics().density;

        double notAchievedProportion = (double) notAchievedCredits/totalCredits;
        double notAchievedDPValue = notAchievedProportion*360;
        int notAchievedPXValue = (int) (notAchievedDPValue * scale + 0.5f);

        double achievedProportion = (double) achievedCredits/totalCredits;
        double achievedDPValue = achievedProportion*360;
        int achievedPXValue = (int) (achievedDPValue * scale + 0.5f);

        double meritProportion = (double) meritCredits/totalCredits;
        double meritDPValue = meritProportion*360;
        int meritPXValue = (int) (meritDPValue * scale + 0.5f);

        double excellenceProportion = (double) excellenceCredits/totalCredits;
        double excellenceDPValue = excellenceProportion*360;
        int excellencePXValue = (int) (excellenceDPValue * scale + 0.5f);

        TextView notAchievedColourView = (TextView) findViewById(R.id.not_achieved_colour);
        notAchievedColourView.setBackgroundColor(Color.rgb(64, 136, 207));
        notAchievedColourView.setWidth(notAchievedPXValue);

        TextView achievedColourView = (TextView) findViewById(R.id.achieved_colour);
        achievedColourView.setBackgroundColor(Color.rgb(22, 116, 206));
        achievedColourView.setWidth(achievedPXValue);

        TextView meritColourView = (TextView) findViewById(R.id.merit_colour);
        meritColourView.setBackgroundColor(Color.rgb(5, 73, 139));
        meritColourView.setWidth(meritPXValue);

        TextView excellenceColourView = (TextView) findViewById(R.id.excellence_colour);
        excellenceColourView.setBackgroundColor(Color.rgb(3, 57, 108));
        excellenceColourView.setWidth(excellencePXValue);

    }


    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.not_achieved_colour:
                SubjectProgressBarActivity.startActivity(this, subjectID, yearID);
                break;
            case R.id.achieved_colour:
                SubjectProgressBarActivity.startActivity(this, subjectID, yearID);
                break;
            case R.id.merit_colour:
                SubjectProgressBarActivity.startActivity(this, subjectID, yearID);
                break;
            case R.id.excellence_colour:
                SubjectProgressBarActivity.startActivity(this, subjectID, yearID);
                break;
            case R.id.add:
                //Topic topicItem = (Topic) topicList.getItemAtPosition(position);
                AddTopicActivity.startActivity(this, subjectID, yearID);
        }
    }


    @Override
    public void onBackPressed() {
        SubjectActivity.startActivity(this, yearID);
    }


    @Override
    protected void onResume() {
        datasource.open();
        super.onResume();
    }


    @Override
    protected void onPause() {
        datasource.close();
        super.onPause();
    }


    public static Intent startActivity(Activity activity, long subjectID, long yearID) {
        Intent intent = new Intent(activity, TopicActivity.class);
        intent.putExtra("EXTRA_SUBJECT_ID", subjectID);
        intent.putExtra("EXTRA_YEAR_ID", yearID);
        activity.startActivity(intent);
        return intent;
    }

}