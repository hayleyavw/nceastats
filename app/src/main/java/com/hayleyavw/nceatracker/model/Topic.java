package com.hayleyavw.nceatracker.model;

public class Topic{
    private long topicID;
    private String topicTitle;
    private int numCredits;
    private String intExtType;
    private String pracFinStatus;
    private String grade;
    private long subjectID;
    private long yearID;

    public long getTopicID() {
        return topicID;
    }

    public void setTopicID(long topicID) {
        this.topicID = topicID;
    }

    public String getTopicTitle(){
        return topicTitle;
    }

    public void setTopicTitle(String topicTitle){
        this.topicTitle = topicTitle;
    }

    public int getNumCredits() {
        return numCredits;
    }

    public void setNumCredits(int numCredits){
        this.numCredits = numCredits;
    }


    public String getIntExtType() {
        return intExtType;
    }

    public void setIntExtType(String intExtType){
        this.intExtType = intExtType;
    }

    public String getPracFinStatus() {
        return pracFinStatus;
    }

    public void setPracFinStatus(String pracFinStatus){
        this.pracFinStatus = pracFinStatus;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade){
        this.grade = grade;
    }

    public long getSubjectID() {
        return subjectID;
    }

    public void setSubjectID(long subjectID) {
        this.subjectID = subjectID;
    }

    public long getYearID() {
        return yearID;
    }

    public void setYearID(long yearID) {
        this.yearID = yearID;
    }

    // Will be used by the ArrayAdapter in the ListView
    @Override
    public String toString() {
        return topicTitle;
    }
} 