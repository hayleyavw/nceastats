package com.hayleyavw.nceatracker.model;

public class Year {
    private long yearID;
    private String date;
    private String yearEndorseStatus;
    private int rankScore;
    private String yearGoal;
    private int UECredits;

    public long getYearID() {
        return yearID;
    }

    public void setYearID(long yearID) {
        this.yearID = yearID;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date){
        this.date = date;
    }

    public String getYearEndorseStatus() {
        return yearEndorseStatus;
    }

    public void setYearEndorseStatus(String yearEndorseStatus) {
        this.yearEndorseStatus = yearEndorseStatus;
    }

    public int getRankScore() {
        return rankScore;
    }

    public void setRankScore(int rankScore) {
        this.rankScore = rankScore;
    }

    public String getYearGoal() {
        return yearGoal;
    }

    public void setYearGoal(String yearGoal) {
        this.yearGoal = yearGoal;
    }

    public int getUECredits() {
        return UECredits;
    }

    public void setUECredits(int UECredits) {
        this.UECredits = UECredits;
    }

    // Will be used by the ArrayAdapter in the ListView
    @Override
    public String toString() {
        return date;
    }
} 