package com.hayleyavw.nceatracker.model;

public class Subject {
    private long subjectID;
    private String subjectName;
    private String subjectEndorseStatus;
    private String subjectLevel;
    private String subjectGoal;
    private long yearID;

    public long getSubjectID() {
        return subjectID;
    }

    public void setSubjectID(long subjectID) {
        this.subjectID = subjectID;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getSubjectEndorseStatus() {
        return subjectEndorseStatus;
    }

    public void setSubjectEndorseStatus(String subjectEndorseStatus) {
        this.subjectEndorseStatus = subjectEndorseStatus;
    }

    public String getSubjectLevel() {
        return subjectLevel;
    }

    public void setSubjectLevel(String subjectLevel) {
        this.subjectLevel = subjectLevel;
    }

    public String getSubjectGoal() {
        return subjectGoal;
    }

    public void setSubjectGoal(String subjectGoal) {
        this.subjectGoal = subjectGoal;
    }

    public long getYearID() {
        return yearID;
    }

    public void setYearID(long yearID) {
        this.yearID = yearID;
    }
    // Will be used by the ArrayAdapter in the ListView
    @Override
    public String toString() {
        return subjectName;
    }
} 