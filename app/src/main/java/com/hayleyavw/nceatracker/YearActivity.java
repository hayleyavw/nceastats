package com.hayleyavw.nceatracker;

import android.content.Intent;
import android.widget.*;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView.OnItemClickListener;
import com.hayleyavw.nceatracker.db.YearsDataSource;
import com.hayleyavw.nceatracker.model.Year;
import com.hayleyavw.nceatracker.ui.YearRowAdapter;

public class YearActivity extends Activity implements OnItemClickListener {

    private YearsDataSource datasource;
    ListView yearList;


    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.year_list_layout);

        datasource = new YearsDataSource(this);
        datasource.open();

        ArrayList<Year> values = datasource.getAllYears();

        yearList = (ListView) findViewById(R.id.yearList);
        yearList.setOnItemClickListener(this);

        // 1. pass context and data to the custom adapter
        YearRowAdapter yearAdapter = new YearRowAdapter(this, values);

        //2. setListAdapter
        yearList.setAdapter(yearAdapter);
    }


    public void onItemClick(AdapterView<?> adapter, View view, int position, long arg) {
        Year yearItem = (Year) yearList.getItemAtPosition(position);
        SubjectActivity.startActivity(this, yearItem.getYearID());
    }


    public void onClick(View view) {
        @SuppressWarnings("unchecked")
        ArrayAdapter<Year> adapter = (ArrayAdapter<Year>) yearList.getAdapter();
        Year year = null;
        switch (view.getId()) {
            case R.id.add:
                AddYearActivity.startActivity(this);
                break;
        }
        adapter.notifyDataSetChanged();
    }


    @Override
    protected void onResume() {
        datasource.open();
        super.onResume();
    }


    @Override
    protected void onPause() {
        datasource.close();
        super.onPause();
    }


    public static Intent startActivity(Activity activity) {
        Intent intent = new Intent(activity, YearActivity.class);
        activity.startActivity(intent);
        return intent;
    }

}