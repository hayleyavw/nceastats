package com.hayleyavw.nceatracker;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import android.text.Editable;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

import com.hayleyavw.nceatracker.db.YearsDataSource;
import com.hayleyavw.nceatracker.model.Year;
import com.hayleyavw.nceatracker.ui.AddYearRowAdapter;

public class AddYearActivity extends Activity {

    private YearsDataSource datasource;
    ListView yearList;
    long id = 0;
    boolean updating = false;
    Year year = null;


    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_year_layout);

        datasource = new YearsDataSource(this);
        datasource.open();

        ArrayList<Year> values = datasource.getAllYears();

        yearList = (ListView) findViewById(R.id.yearList);

        AddYearRowAdapter yearAdapter = new AddYearRowAdapter(this, values);

        yearList.setAdapter(yearAdapter);
    }


    public void onClick(View view) {
        @SuppressWarnings("unchecked")
        Button buttonText = (Button)findViewById(R.id.save);
        EditText edit = (EditText) findViewById(R.id.enter_year);
        Editable editable = edit.getText();
        String allTheText = editable.toString();
        /*if (edit.length() > 0) {
            updating = true;
        }*/
        InputMethodManager imm = (InputMethodManager)getSystemService(this.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(edit.getWindowToken(), 0);
        edit.clearFocus();
        ArrayAdapter<Year> adapter = (ArrayAdapter<Year>) yearList.getAdapter();
        final int position = yearList.getPositionForView((View) view.getParent());
        switch (view.getId()) {
            case R.id.save:
                if (allTheText.length() < 4) {
                    break;
                }
                if (updating == true) {
                    System.out.println("here" + id);
                    adapter.remove(year);
                    year = datasource.editYear(id, allTheText);
                    adapter.add(year);
                    adapter.notifyDataSetChanged();
                    edit.setText("");
                    updating = false;
                } else {
                    year = datasource.createYear(0, allTheText, "Achieved", 123, "Merit", 100);
                    adapter.add(year);
                    edit.setText("");
                    updating = false;
                }
                break;
            case R.id.edit:
                buttonText.setText("Save Changes");
                updating = true;
                System.out.println(updating);
                year = (Year) yearList.getAdapter().getItem(position);
                //adapter.remove(year);
                String date = year.getDate();
                id = year.getYearID();
                System.out.println("here edit" + id);
                edit.setText(date);
                break;
            case R.id.delete:
                year = (Year) yearList.getAdapter().getItem(position);
                adapter.remove(year);
                adapter.notifyDataSetChanged();
                datasource.deleteYear(year);
                break;
            case R.id.done:
                if (updating == true) {
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
                    builder1.setMessage("You have unsaved changes");
                    builder1.setCancelable(true);
                    builder1.setPositiveButton("Continue anyway",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                    Intent i = new Intent(getBaseContext(), YearActivity.class);
                                    startActivity(i);
                                }
                            });
                    builder1.setNegativeButton("Go back",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert11 = builder1.create();
                    alert11.show();
                } else {
                    YearActivity.startActivity(this);
                    break;
                }
        }
        adapter.notifyDataSetChanged();
    }


    @Override
    protected void onResume() {
        datasource.open();
        super.onResume();
    }


    @Override
    protected void onPause() {
        datasource.close();
        super.onPause();
    }


    public static Intent startActivity(Activity activity) {
        Intent intent = new Intent(activity, AddYearActivity.class);
        activity.startActivity(intent);
        return intent;
    }

}