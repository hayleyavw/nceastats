package com.hayleyavw.nceatracker;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import com.hayleyavw.nceatracker.db.TopicsDataSource;
import com.hayleyavw.nceatracker.model.Topic;
import com.hayleyavw.nceatracker.ui.AddTopicRowAdapter;

import java.util.ArrayList;


public class AddTopicActivity extends Activity {

    private TopicsDataSource datasource;
    ListView topicList;
    long subjectID;
    long yearID;
    String intExt;
    String pracFin;
    String grade;


    long id = 0;
    boolean updating = false;
    Topic topic = null;

    private RadioGroup FirstGradeGroup;
    private RadioGroup SecondGradeGroup;
    private RadioButton gradeButton;
    private RadioGroup IntExtGroup;
    private RadioButton intExtButton;
    private RadioGroup PracFinGroup;
    private RadioButton pracFinButton;

    private boolean isChecking = true;
    private int mCheckedId;


    @Override
    public void onCreate(Bundle savedInstanceState) {

        subjectID = getIntent().getLongExtra("EXTRA_SUBJECT_ID", -1);
        yearID = getIntent().getLongExtra("EXTRA_YEAR_ID", -1);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_topic_layout);

        datasource = new TopicsDataSource(this);
        datasource.open();

        ArrayList<Topic> values = datasource.getTopicsBySubject(subjectID);

        topicList = (ListView) findViewById(R.id.topicList);

        AddTopicRowAdapter topicAdapter = new AddTopicRowAdapter(this, values);

        topicList.setAdapter(topicAdapter);

        FirstGradeGroup = (RadioGroup) findViewById(R.id.first_grades);
        SecondGradeGroup = (RadioGroup) findViewById(R.id.second_grades);
        IntExtGroup = (RadioGroup) findViewById(R.id.int_ext_buttons);
        PracFinGroup = (RadioGroup) findViewById(R.id.prac_fin_buttons);

        FirstGradeGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId != -1 && isChecking) {
                    isChecking = false;
                    SecondGradeGroup.clearCheck();
                    mCheckedId = checkedId;
                }
                isChecking = true;
            }
        });

        SecondGradeGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId != -1 && isChecking) {
                    isChecking = false;
                    FirstGradeGroup.clearCheck();
                    mCheckedId = checkedId;
                }
                isChecking = true;
            }
        });

    }


    public void onClick(View view) {
        @SuppressWarnings("unchecked")
        Button buttonText = (Button)findViewById(R.id.save);
        EditText edit = (EditText) findViewById(R.id.enter_topic_title);
        Editable editable = edit.getText();
        String topicTitle = editable.toString();
        EditText editNum = (EditText) findViewById(R.id.enter_num_credits);
        String num = editNum.getText().toString();
        InputMethodManager imm = (InputMethodManager)getSystemService(this.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(edit.getWindowToken(), 0);
        edit.clearFocus();
        editNum.clearFocus();
        /*
        if (edit.length() > 0) {
            updating = true;
        } else if (pracFin != null) {
            updating = true;
        } else if (intExt == null) {
            updating = true;
        } else if (grade == null) {
            updating = true;
        } else if (num.length() > 0) {
            updating = true;
        }*/
        ArrayAdapter<Topic> adapter = (ArrayAdapter<Topic>) topicList.getAdapter();
        final int position = topicList.getPositionForView((View) view.getParent());
        switch (view.getId()) {
            case R.id.save:
                buttonText.setText("Add");
                if (num.length() < 1) {
                    break;
                } else if (pracFin == null) {
                    System.out.println("pracfin");
                    break;
                } else if (intExt == null) {
                    System.out.println("intext");
                    break;
                } else if (grade == null) {
                    System.out.println("grade");
                    break;
                }
                System.out.println("made it");
                if (updating == true) {
                    adapter.remove(topic);
                    int numCredits = Integer.parseInt(num);
                    topic = datasource.editTopic(id, topicTitle, numCredits, intExt, pracFin, grade);
                    adapter.add(topic);
                    adapter.notifyDataSetChanged();
                    edit.setText("");
                    editNum.setText("");
                    edit.clearFocus();
                    editNum.clearFocus();
                    FirstGradeGroup.clearCheck();
                    SecondGradeGroup.clearCheck();
                    IntExtGroup.clearCheck();
                    PracFinGroup.clearCheck();
                    updating = false;
                } else {
                    int numCredits = Integer.parseInt(num);
                    Topic topic = datasource.createTopic(0, topicTitle, numCredits, intExt, pracFin, grade, subjectID, yearID);
                    adapter.add(topic);
                    edit.setText("");
                    editNum.setText("");
                    FirstGradeGroup.clearCheck();
                    SecondGradeGroup.clearCheck();
                    IntExtGroup.clearCheck();
                    PracFinGroup.clearCheck();
                }
                pracFin = null;
                intExt = null;
                grade = null;
                break;
            case R.id.edit:
                buttonText.setText("Save Changes");
                updating = true;
                topic = (Topic) topicList.getAdapter().getItem(position);
                String title = topic.getTopicTitle();
                int credits = topic.getNumCredits();
                intExt = topic.getIntExtType();
                pracFin = topic.getPracFinStatus();
                grade = topic.getGrade();
                id = topic.getTopicID();
                edit.setText(title);
                editNum.setText(Integer.toString(credits));
                if (intExt.equals("Internal")) {
                    intExtButton = (RadioButton)findViewById(R.id.internal);
                } else if (intExt.equals("External")) {
                    intExtButton = (RadioButton)findViewById(R.id.external);
                } else {
                    System.out.println("failed intext");
                }
                intExtButton.setChecked(true);
                if (pracFin.equals("Practice")) {
                    pracFinButton = (RadioButton)findViewById(R.id.practice_grade);
                } else if (pracFin.equals("Final")) {
                    pracFinButton = (RadioButton)findViewById(R.id.final_grade);
                } else {
                    System.out.println("failed pracfin");
                }
                pracFinButton.setChecked(true);
                if (grade.equals("Not Achieved")) {
                    gradeButton = (RadioButton)findViewById(R.id.not_achieved);
                } else if (grade.equals("Achieved")) {
                    gradeButton = (RadioButton)findViewById(R.id.achieved);
                } else if (grade.equals("Merit")) {
                    gradeButton = (RadioButton)findViewById(R.id.merit);
                } else if (grade.equals("Excellence")) {
                    gradeButton = (RadioButton)findViewById(R.id.excellence);
                } else {
                    System.out.println("failed grade");
                }
                gradeButton.setChecked(true);
                break;
            case R.id.delete:
                Topic topic = (Topic) topicList.getAdapter().getItem(position);
                adapter.remove(topic);
                adapter.notifyDataSetChanged();
                datasource.deleteTopic(topic);
                break;
            case R.id.done:
                if (updating == true) {
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
                    builder1.setMessage("You have unsaved changes");
                    builder1.setCancelable(true);
                    builder1.setPositiveButton("Continue anyway",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                    Intent i = new Intent(getBaseContext(), TopicActivity.class);
                                    i.putExtra("EXTRA_SUBJECT_ID", subjectID);
                                    i.putExtra("EXTRA_YEAR_ID", yearID);
                                    startActivity(i);
                                }
                            });
                    builder1.setNegativeButton("Go back",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert11 = builder1.create();
                    alert11.show();
                } else {
                    TopicActivity.startActivity(this, subjectID, yearID);
                    break;
                }
        }
        adapter.notifyDataSetChanged();
    }


    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.internal:
                if (checked)
                    intExt = "Internal";
                break;
            case R.id.external:
                if (checked)
                    intExt = "External";
                break;
            case R.id.practice_grade:
                if (checked)
                    pracFin = "Practice";
                break;
            case R.id.final_grade:
                if (checked)
                    pracFin = "Final";
                break;
            case R.id.not_achieved:
                if (checked)
                    grade = "Not Achieved";
                break;
            case R.id.achieved:
                if (checked)
                    grade = "Achieved";
                break;
            case R.id.merit:
                if (checked)
                    grade = "Merit";
                break;
            case R.id.excellence:
                if (checked)
                    grade = "Excellence";
                break;
        }
    }


    @Override
    protected void onResume() {
        datasource.open();
        super.onResume();
    }


    @Override
    protected void onPause() {
        datasource.close();
        super.onPause();
    }


    public static Intent startActivity(Activity activity, long subjectID, long yearID) {
        Intent intent = new Intent(activity, AddTopicActivity.class);
        intent.putExtra("EXTRA_SUBJECT_ID", subjectID);
        intent.putExtra("EXTRA_YEAR_ID", yearID);
        activity.startActivity(intent);
        return intent;
    }

}