package com.hayleyavw.nceatracker;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;

import com.hayleyavw.nceatracker.db.SubjectsDataSource;
import com.hayleyavw.nceatracker.model.Subject;
import android.widget.ArrayAdapter;
import com.hayleyavw.nceatracker.ui.AddSubjectRowAdapter;

import java.util.ArrayList;



public class AddSubjectActivity extends Activity {

    private SubjectsDataSource datasource;
    ListView subjectList;
    long yearID;
    String level;
    long id = 0;
    boolean updating = false;
    Subject subject = null;

    private RadioGroup LevelGroup;
    public RadioButton levelButton;


    @Override
    public void onCreate(Bundle savedInstanceState) {

        yearID = getIntent().getLongExtra("EXTRA_YEAR_ID", -1);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_subject_layout);

        datasource = new SubjectsDataSource(this);
        datasource.open();

        ArrayList<Subject> values = datasource.getSubjectsByYear(yearID);

        subjectList = (ListView) findViewById(R.id.subjectList);

        // 1. pass context and data to the custom adapter
        AddSubjectRowAdapter subjectAdapter = new AddSubjectRowAdapter(this, values);

        //2. setListAdapter
        subjectList.setAdapter(subjectAdapter);

        LevelGroup = (RadioGroup) findViewById(R.id.level_buttons);
    }


    public void onClick(View view) {
        @SuppressWarnings("unchecked")
        Button buttonText = (Button)findViewById(R.id.save);
        EditText edit = (EditText) findViewById(R.id.enter_subject);
        Editable editable = edit.getText();
        /*if (edit.length() > 0) {
            updating = true;
        } else if (level != null) {
            updating = true;
        }*/
        InputMethodManager imm = (InputMethodManager)getSystemService(this.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(edit.getWindowToken(), 0);
        edit.clearFocus();
        String allTheText = editable.toString();
        ArrayAdapter<Subject> adapter = (ArrayAdapter<Subject>) subjectList.getAdapter();
        final int position = subjectList.getPositionForView((View) view.getParent());
        switch (view.getId()) {
            case R.id.save:
                System.out.println("level = " + level);
                if (level == null) {
                    break;
                }
                if (updating == true) {
                    System.out.println("here" + id);
                    adapter.remove(subject);
                    subject = datasource.editSubject(id, allTheText, level);
                    adapter.add(subject);
                    adapter.notifyDataSetChanged();
                    edit.setText("");
                    LevelGroup.clearCheck();
                    updating = false;
                } else {
                    subject = datasource.createSubject(0, allTheText, "Achieved", level, "Merit", yearID);
                    adapter.add(subject);
                    edit.setText("");
                    LevelGroup.clearCheck();
                }
                level = null;
                break;
            case R.id.edit:
                buttonText.setText("Save Changes");
                updating = true;
                subject = (Subject) subjectList.getAdapter().getItem(position);
                String name = subject.getSubjectName();
                id = subject.getSubjectID();
                edit.setText(name);
                level = subject.getSubjectLevel();
                if (level.equals("1")){
                    levelButton = (RadioButton)findViewById(R.id.level_one);
                } else if (level.equals("2")) {
                    levelButton = (RadioButton)findViewById(R.id.level_two);
                } else if (level.equals("3")) {
                    levelButton = (RadioButton)findViewById(R.id.level_three);
                }
                levelButton.setChecked(true);
                break;
            case R.id.delete:
                subject = (Subject) subjectList.getAdapter().getItem(position);
                adapter.remove(subject);
                adapter.notifyDataSetChanged();
                datasource.deleteSubject(subject);
                break;
            case R.id.done:
                if (updating == true) {
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
                    builder1.setMessage("You have unsaved changes");
                    builder1.setCancelable(true);
                    builder1.setPositiveButton("Continue anyway",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                    Intent i = new Intent(getBaseContext(), SubjectActivity.class);
                                    i.putExtra("EXTRA_YEAR_ID", yearID);
                                    startActivity(i);
                                }
                            });
                    builder1.setNegativeButton("Go back",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert11 = builder1.create();
                    alert11.show();
                } else {
                    SubjectActivity.startActivity(this, yearID);
                    break;
                }
        }
        adapter.notifyDataSetChanged();
    }


    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.level_one:
                if (checked)
                    System.out.println("one");
                    level = "1";
                    break;
            case R.id.level_two:
                if (checked)
                    level = "2";
                    break;
            case R.id.level_three:
                if (checked)
                    level = "3";
                    break;
        }
        System.out.println(level);
    }


    @Override
    protected void onResume() {
        datasource.open();
        super.onResume();
    }


    @Override
    protected void onPause() {
        datasource.close();
        super.onPause();
    }


    public static Intent startActivity(Activity activity, long yearID) {
        Intent intent = new Intent(activity, AddSubjectActivity.class);
        intent.putExtra("EXTRA_YEAR_ID", yearID);
        activity.startActivity(intent);
        return intent;
    }

}