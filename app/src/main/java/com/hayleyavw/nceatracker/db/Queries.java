//PRETTY SURE THIS FILE CURRENTLY DOES NOTHING

package com.hayleyavw.nceatracker.db;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import com.hayleyavw.nceatracker.model.Subject;

public class Queries {

    private SQLiteDatabase database;
    private MySQLiteHelper dbHelper;
    private String[] allColumns = { "topicID", "topicTitle", "numCredits", "intExtType", "pracFinStatus",
            "grade", "subjectID", "yearID" };
    Subject subject;
    private Queries datasource;


    public Queries(Context context) {
        dbHelper = new MySQLiteHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }


    private Subject cursorToSubject(Cursor cursor) {
        Subject subject = new Subject();
        subject.setSubjectID(cursor.getLong(0));
        subject.setSubjectName(cursor.getString(1));
        subject.setSubjectEndorseStatus(cursor.getString(2));
        subject.setSubjectLevel(cursor.getString(3));
        subject.setSubjectGoal(cursor.getString(4));
        subject.setYearID(cursor.getLong(5));
        return subject;
    }

}
