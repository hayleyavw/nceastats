package com.hayleyavw.nceatracker.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class MySQLiteHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "studentCredits.db";
    private static final int DATABASE_VERSION = 1;

    // Database creation sql statement
    private static final String YEAR_TABLE_CREATE = "CREATE TABLE " +
            "year_table (yearID INTEGER PRIMARY KEY, date TEXT, yearEndorseStatus TEXT, " +
            "rankScore INTEGER, yearGoal TEXT, UECredits INTEGER)";

    private static final String SUBJECT_TABLE_CREATE = "CREATE TABLE " +
            "subject_table (subjectID INTEGER PRIMARY KEY, subjectName TEXT, subjectEndorseStatus TEXT, " +
            "subjectLevel TEXT, subjectGoal TEXT, yearID INTEGER REFERENCES year_table(yearID))";

    private static final String TOPIC_TABLE_CREATE = "CREATE TABLE " +
            "topic_table (topicID INTEGER PRIMARY KEY, topicTitle, numCredits INTEGER, " +
            "intExtType STRING, pracFinStatus STRING, grade STRING," +
            "subjectID INTEGER REFERENCES subject_table(subjectID), " +
            "yearID INTEGER REFERENCES year_table(yearID))";

    public MySQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    //creates tables on create
    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL(YEAR_TABLE_CREATE);
        database.execSQL(SUBJECT_TABLE_CREATE);
        database.execSQL(TOPIC_TABLE_CREATE);
        Log.i("test", "making db?");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(MySQLiteHelper.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS year_table");
        onCreate(db);
    }

} 