package com.hayleyavw.nceatracker.db;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import com.hayleyavw.nceatracker.model.Year;

public class YearsDataSource {

    private SQLiteDatabase database;
    private MySQLiteHelper dbHelper;
    private String[] allColumns = { "yearID", "date", "yearEndorseStatus", "rankScore", "yearGoal", "UECredits" };

    public YearsDataSource(Context context) {
        dbHelper = new MySQLiteHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }


    public Year createYear(int yearID, String date, String yearEndorseStatus, int rankScore, String yearGoal, int UECredits) {
        ContentValues values = new ContentValues();

        values.put("date", date);
        values.put("yearEndorseStatus", yearEndorseStatus);
        values.put("rankScore", rankScore);
        values.put("yearGoal", yearGoal);
        values.put("UECredits", UECredits);

        long insertID = database.insert("year_table", null, values);
        Cursor cursor = database.query("year_table",
                allColumns, "yearID" + " = " + insertID, null, null, null, null);
        System.out.println("Year inserted with ID: " + insertID);
        cursor.moveToFirst();
        Year newYear = cursorToYear(cursor);
        cursor.close();
        return newYear;
    }


    public void deleteYear(Year year) {
        long ID = year.getYearID();
        System.out.println("Year deleted with ID: " + ID);
        database.delete("year_table", "yearID" + " = " + ID, null);
        database.delete("subject_table", "yearID" + " = " + ID, null);
        database.delete("topic_table", "yearID = " + ID, null);
    }


    public Year editYear(long yearID, String newDate) {
        ContentValues date = new ContentValues();
        date.put("date", newDate);
        database.update("year_table", date, "yearID = " + yearID, null);
        Cursor cursor = database.query("year_table",
                allColumns, "yearID" + " = " + yearID, null, null, null, null);
        cursor.moveToFirst();
        Year updatedYear = cursorToYear(cursor);
        cursor.close();
        return updatedYear;
    }


    public ArrayList<Year> getAllYears() {
        ArrayList<Year> years = new ArrayList<Year>();

        Cursor cursor = database.query("year_table", allColumns, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Year year = cursorToYear(cursor);
            years.add(year);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return years;
    }


    private Year cursorToYear(Cursor cursor) {
        Year year = new Year();
        year.setYearID(cursor.getLong(0));
        year.setDate(cursor.getString(1));
        year.setYearEndorseStatus(cursor.getString(2));
        year.setRankScore(cursor.getInt(3));
        year.setYearGoal(cursor.getString(4));
        year.setUECredits(cursor.getInt(5));
        return year;
    }
}