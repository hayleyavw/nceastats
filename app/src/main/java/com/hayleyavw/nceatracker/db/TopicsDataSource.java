package com.hayleyavw.nceatracker.db;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import com.hayleyavw.nceatracker.model.Subject;
import com.hayleyavw.nceatracker.model.Topic;

public class TopicsDataSource {

    private SQLiteDatabase database;
    private MySQLiteHelper dbHelper;
    private String[] allColumns = { "topicID", "topicTitle", "numCredits", "intExtType", "pracFinStatus",
            "grade", "subjectID", "yearID" };
    int numCredits;
    Subject subject;

    public TopicsDataSource(Context context) {
        dbHelper = new MySQLiteHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }


    public Topic createTopic(long topicID, String topicTitle, int numCredits, String intExtType,
                             String pracFinStatus, String grade, long subjectID, long yearID) {
        ContentValues values = new ContentValues();

        values.put("topicTitle", topicTitle);
        values.put("numCredits", numCredits);
        values.put("intExtType", intExtType);
        values.put("pracFinStatus", pracFinStatus);
        values.put("grade", grade);
        values.put("subjectID", subjectID);
        values.put("yearID", yearID);

        long insertID = database.insert("topic_table", null, values);
        Cursor cursor = database.query("topic_table",
                allColumns, "topicID" + " = " + insertID, null, null, null, null);
        System.out.println("Topic inserted with ID: " + insertID);
        cursor.moveToFirst();
        Topic newTopic = cursorToTopic(cursor);
        cursor.close();
        return newTopic;
    }


    public void deleteTopic(Topic topic) {
        long ID = topic.getTopicID();
        System.out.println("Topic deleted with ID: " + ID);
        database.delete("topic_table", "topicID" + " = " + ID, null);
    }


    public Topic editTopic(long topicID, String newTitle, int newNumCredits, String newIntExt, String newPracFin, String newGrade) {
        ContentValues topicValues = new ContentValues();
        topicValues.put("topicTitle", newTitle);
        topicValues.put("numCredits", newNumCredits);
        topicValues.put("intExtType", newIntExt);
        topicValues.put("pracFinStatus", newPracFin);
        topicValues.put("grade", newGrade);
        database.update("topic_table", topicValues, "topicID = " + topicID, null);
        Cursor cursor = database.query("topic_table", allColumns, "topicID = " + topicID, null, null, null, null);
        cursor.moveToFirst();
        Topic updatedTopic = cursorToTopic(cursor);
        cursor.close();
        return updatedTopic;
    }


    public ArrayList<Topic> getAllTopics() {
        ArrayList<Topic> topics = new ArrayList<Topic>();

        Cursor cursor = database.query("topic_table", allColumns, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Topic topic = cursorToTopic(cursor);
            topics.add(topic);
            cursor.moveToNext();
        }
        cursor.close();
        return topics;
    }


    public String calculateEndorsementStatus(long subjectID, String pracFinStatus) {
        String endorsementStatus;

        if (pracFinStatus.equals("Null")) {
            int numExcInt = getNumCreditsByGradeByType("'Excellence'", "'Internal'", subjectID);
            int numExcExt = getNumCreditsByGradeByType("'Excellence'", "'External'", subjectID);
            int totalExc = numExcInt + numExcExt;

            if (numExcInt >= 3 && numExcExt >= 3 && numExcInt + numExcInt >= 14) {
                endorsementStatus = "Excellence";
            } else {
                int numMerInt = getNumCreditsByGradeByType("'Merit'", "'Internal'", subjectID);
                int numMerExt = getNumCreditsByGradeByType("'Merit'", "'External'", subjectID);
                int totalMer = numMerInt + numMerExt + totalExc;
                if (numMerInt + numExcInt >= 3 && numMerExt + numExcExt >= 3 && totalMer >= 14) {
                    endorsementStatus = "Merit";
                } else {
                    endorsementStatus = "Not endorsed";
                }
            }
        } else {
            int numExcInt = getNumFinalCreditsByGradeByType("'Excellence'", "'Internal'", subjectID);
            int numExcExt = getNumFinalCreditsByGradeByType("'Excellence'", "'External'", subjectID);
            int totalExc = numExcInt + numExcExt;

            if (numExcInt >= 3 && numExcExt >= 3 && numExcInt + numExcInt >= 14) {
                endorsementStatus = "Excellence";
            } else {
                int numMerInt = getNumFinalCreditsByGradeByType("'Merit'", "'Internal'", subjectID);
                int numMerExt = getNumFinalCreditsByGradeByType("'Merit'", "'External'", subjectID);
                int totalMer = numMerInt + numMerExt + totalExc;
                if (numMerInt + numExcInt >= 3 && numMerExt + numExcExt >= 3 && totalMer >= 14) {
                    endorsementStatus = "Merit";
                } else {
                    endorsementStatus = "Not endorsed";
                }
            }
        }
        return endorsementStatus;
    }


    public int getNumFinalCreditsByGradeByType(String grade, String intExtStatus, long subjectID) {
        int numCredits = 0;

        ArrayList<Topic> topics = new ArrayList<Topic>();
        String[] columns = { "topicID", "topicTitle", "numCredits", "intExtType", "pracFinStatus",
                "grade", "subjectID", "yearID" };
        Cursor cursor = database.query("topic_table", columns, "subjectID = " + subjectID + " AND grade = " + grade + " AND intExtType = " + intExtStatus + " AND pracFinStatus = 'Final'", null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Topic topic = cursorToTopic(cursor);
            numCredits = numCredits + topic.getNumCredits();
            topics.add(topic);
            cursor.moveToNext();
        }
        cursor.close();
        return numCredits;
    }


    public int getNumCreditsByGradeByType(String grade, String intExtStatus, long subjectID) {
        int numCredits = 0;

        ArrayList<Topic> topics = new ArrayList<Topic>();
        String[] columns = { "topicID", "topicTitle", "numCredits", "intExtType", "pracFinStatus",
                "grade", "subjectID", "yearID" };
        Cursor cursor = database.query("topic_table", columns, "subjectID = " + subjectID + " AND grade = " + grade + " AND intExtType = " + intExtStatus, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Topic topic = cursorToTopic(cursor);
            numCredits = numCredits + topic.getNumCredits();
            topics.add(topic);
            cursor.moveToNext();
        }
        cursor.close();
        return numCredits;
    }


    public ArrayList<Topic> getTopicsBySubject(long subjectID) {
        ArrayList<Topic> topics = new ArrayList<Topic>();

        Cursor cursor = database.query("topic_table", allColumns, "subjectID = " + subjectID, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Topic topic = cursorToTopic(cursor);
            topics.add(topic);
            cursor.moveToNext();
        }
        cursor.close();
        return topics;
    }



    public int getAllGradeCreditsBySubject(long subjectID, String grade) {

        int numCredits = 0;

        ArrayList<Topic> topics = new ArrayList<Topic>();
        String[] columns = { "topicID", "topicTitle", "numCredits", "intExtType", "pracFinStatus",
                "grade", "subjectID", "yearID" };
        Cursor cursor = database.query("topic_table", columns, "subjectID = " + subjectID + " AND grade = " + grade, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Topic topic = cursorToTopic(cursor);
            numCredits = numCredits + topic.getNumCredits();
            topics.add(topic);
            cursor.moveToNext();
        }
        cursor.close();
        return numCredits;
    }


    public int getFinalGradeCreditsBySubject(long subjectID, String grade) {

        int numCredits = 0;
        String pracFinStatus = "'Final'";

        ArrayList<Topic> topics = new ArrayList<Topic>();
        String[] columns = { "topicID", "topicTitle", "numCredits", "intExtType", "pracFinStatus",
                "grade", "subjectID", "yearID" };
        Cursor cursor = database.query("topic_table", columns, "subjectID = " + subjectID + " AND grade = " + grade + " AND pracFinStatus = " + pracFinStatus, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Topic topic = cursorToTopic(cursor);
            numCredits = numCredits + topic.getNumCredits();
            topics.add(topic);
            cursor.moveToNext();
        }
        cursor.close();
        return numCredits;

    }


    public int getAllCreditsBySubjectByIntExtType(long subjectID, String type) {

        int numCredits = 0;
        String intExtStatus = type;

        ArrayList<Topic> topics = new ArrayList<Topic>();
        String[] columns = { "topicID", "topicTitle", "numCredits", "intExtType", "pracFinStatus",
                "grade", "subjectID", "yearID" };
        Cursor cursor = database.query("topic_table", columns, "subjectID = " + subjectID + " AND intExtType = " + intExtStatus, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Topic topic = cursorToTopic(cursor);
            numCredits = numCredits + topic.getNumCredits();
            topics.add(topic);
            cursor.moveToNext();
        }
        cursor.close();
        return numCredits;
    }


    public int getFinalCreditsBySubjectByIntExtType(long subjectID, String type) {

        int numCredits = 0;
        String intExtStatus = type;

        ArrayList<Topic> topics = new ArrayList<Topic>();
        String[] columns = { "topicID", "topicTitle", "numCredits", "intExtType", "pracFinStatus",
                "grade", "subjectID", "yearID" };
        Cursor cursor = database.query("topic_table", columns, "subjectID = " + subjectID + " AND intExtType = " + intExtStatus + " AND pracFinStatus = 'Final'", null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Topic topic = cursorToTopic(cursor);
            numCredits = numCredits + topic.getNumCredits();
            topics.add(topic);
            cursor.moveToNext();
        }
        cursor.close();
        return numCredits;
    }


    public String getSubjectNameTitle(long subjectID) {
        String[] nameColumn = { "subjectName" };
        Cursor nameCursor = database.query("subject_table", nameColumn, "subjectID = " + subjectID, null, null, null, null);
        nameCursor.moveToFirst();
        subject = cursorToSubject(nameCursor);
        String subjectName = subject.getSubjectName();
        String[] levelColumn = { "subjectLevel" };
        Cursor levelCursor = database.query("subject_table", levelColumn, "subjectID = " + subjectID, null, null, null, null);
        levelCursor.moveToFirst();
        subject = cursorToSubject(levelCursor);
        String subjectLevel = subject.getSubjectLevel();
        // make sure to close the cursor
        levelCursor.close();
        return "Level " + subjectLevel + " " + subjectName;
    }


    private Topic cursorToTopic(Cursor cursor) {
        Topic topic = new Topic();
        topic.setTopicID(cursor.getLong(0));
        topic.setTopicTitle(cursor.getString(1));
        topic.setNumCredits(cursor.getInt(2));
        topic.setIntExtType(cursor.getString(3));
        topic.setPracFinStatus(cursor.getString(4));
        topic.setGrade(cursor.getString(5));
        topic.setSubjectID(cursor.getLong(6));
        topic.setYearID(cursor.getLong(7));
        return topic;
    }


    private Subject cursorToSubject(Cursor cursor) {
        Subject subject = new Subject();
        //subject.setSubjectID(cursor.getLong(0));
        subject.setSubjectName(cursor.getString(0));
        //subject.setSubjectEndorseStatus(cursor.getString(2));
        subject.setSubjectLevel(cursor.getString(0));
        //subject.setSubjectGoal(cursor.getString(4));
        //subject.setYearID(cursor.getLong(5));
        return subject;
    }

}