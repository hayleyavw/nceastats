package com.hayleyavw.nceatracker.db;

import java.util.ArrayList;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import com.hayleyavw.nceatracker.model.Subject;
import com.hayleyavw.nceatracker.model.Topic;
import com.hayleyavw.nceatracker.model.Year;

public class SubjectsDataSource {

    private SQLiteDatabase database;
    private MySQLiteHelper dbHelper;
    private String[] allColumns = {"subjectID", "subjectName", "subjectEndorseStatus", "subjectLevel", "subjectGoal", "yearID"};

    Year year;

    public SubjectsDataSource(Context context) {
        dbHelper = new MySQLiteHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }


    public Subject createSubject(int subjectID, String subjectName, String subjectEndorseStatus, String subjectLevel, String subjectGoal, long yearID) {
        ContentValues values = new ContentValues();

        values.put("subjectName", subjectName);
        values.put("subjectEndorseStatus", subjectEndorseStatus);
        values.put("subjectLevel", subjectLevel);
        values.put("subjectGoal", subjectGoal);
        values.put("yearID", yearID);

        long insertID = database.insert("subject_table", null, values);
        Cursor cursor = database.query("subject_table",
                allColumns, "subjectID" + " = " + insertID, null, null, null, null);
        System.out.println("Subject inserted with ID: " + insertID);
        cursor.moveToFirst();
        Subject newSubject = cursorToSubject(cursor);
        cursor.close();
        return newSubject;
    }


    public void deleteSubject(Subject subject) {
        long ID = subject.getSubjectID();
        System.out.println("Subject deleted with ID: " + ID);
        database.delete("subject_table", "subjectID" + " = " + ID, null);
        database.delete("topic_table", "subjectID = " + ID, null);
    }


    public Subject editSubject(long subjectID, String newName, String level) {
        ContentValues subjectValues = new ContentValues();
        subjectValues.put("subjectName", newName);
        subjectValues.put("subjectLevel", level);
        database.update("subject_table", subjectValues, "subjectID = " + subjectID, null);
        Cursor cursor = database.query("subject_table",
                allColumns, "subjectID" + " = " + subjectID, null, null, null, null);
        cursor.moveToFirst();
        Subject updatedSubject = cursorToSubject(cursor);
        cursor.close();
        return updatedSubject;
    }


    public ArrayList<Subject> getAllSubjects() {
        ArrayList<Subject> subjects = new ArrayList<Subject>();

        Cursor cursor = database.query("subject_table", allColumns, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Subject subject = cursorToSubject(cursor);
            subjects.add(subject);
            cursor.moveToNext();
        }
        cursor.close();
        return subjects;
    }


    public String calculateEndorsementStatus(long yearID, String pracFinStatus) {

        String endorsementStatus;

        if (pracFinStatus == "'Final'") {

            int totalExc = getFinalGradeCreditsByYear(yearID, "'Excellence'");

            if (totalExc >= 50) {
                endorsementStatus = "Excellence";
            } else {
                int totalMer = getFinalGradeCreditsByYear(yearID, "'Merit'");
                if ((totalMer + totalExc) >= 50) {
                    endorsementStatus = "Merit";
                } else {
                    endorsementStatus = "Not endorsed";
                }
            }

        } else {

            int totalExc = getGradeCreditsByYear(yearID, "'Excellence'");

            if (totalExc >= 50) {
                endorsementStatus = "Excellence";
            } else {
                int totalMer = getGradeCreditsByYear(yearID, "'Merit'");
                if ((totalMer + totalExc) >= 50) {
                    endorsementStatus = "Merit";
                } else {
                    endorsementStatus = "Not endorsed";
                }
            }
        }
        return endorsementStatus;
    }


    public ArrayList<Subject> getSubjectsByYear(long yearID) {
        ArrayList<Subject> subjects = new ArrayList<Subject>();

        Cursor cursor = database.query("subject_table", allColumns, "yearID = " + yearID, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Subject subject = cursorToSubject(cursor);
            subjects.add(subject);
            cursor.moveToNext();
        }
        cursor.close();
        return subjects;
    }


    public int getAllCreditsByYear(long yearID) {

        int numCredits = 0;

        ArrayList<Topic> topics = new ArrayList<Topic>();
        String[] columns = { "topicID", "topicTitle", "numCredits", "intExtType", "pracFinStatus",
                "grade", "subjectID", "yearID" };
        Cursor cursor = database.query("topic_table", columns, "yearID = " + yearID, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Topic topic = cursorToTopic(cursor);
            numCredits = numCredits + topic.getNumCredits();
            topics.add(topic);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();

        return numCredits;
    }


    public int getGradeCreditsByYear(long yearID, String grade) {

        int numCredits = 0;

        ArrayList<Topic> topics = new ArrayList<Topic>();
        String[] columns = { "topicID", "topicTitle", "numCredits", "intExtType", "pracFinStatus",
                "grade", "subjectID", "yearID" };
        Cursor cursor = database.query("topic_table", columns, "yearID = " + yearID + " AND grade = " + grade, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Topic topic = cursorToTopic(cursor);
            numCredits = numCredits + topic.getNumCredits();
            topics.add(topic);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return numCredits;
    }


    public int getFinalGradeCreditsByYear(long yearID, String grade) {

        int numCredits = 0;
        String pracFinStatus = "'Final'";

        ArrayList<Topic> topics = new ArrayList<Topic>();
        String[] columns = { "topicID", "topicTitle", "numCredits", "intExtType", "pracFinStatus",
                "grade", "subjectID", "yearID" };
        Cursor cursor = database.query("topic_table", columns, "yearID = " + yearID + " AND grade = " + grade + " AND pracFinStatus = " + pracFinStatus, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Topic topic = cursorToTopic(cursor);
            numCredits = numCredits + topic.getNumCredits();
            topics.add(topic);
            cursor.moveToNext();
        }
        cursor.close();
        return numCredits;

    }


    public int getAllCreditsByYearByIntExtType(long yearID, String type) {

        int numCredits = 0;
        String intExtStatus = type;
        String[] columns = { "topicID", "topicTitle", "numCredits", "intExtType", "pracFinStatus",
                "grade", "subjectID", "yearID" };
        ArrayList<Topic> topics = new ArrayList<Topic>();
        Cursor cursor = database.query("topic_table", columns, "yearID = " + yearID + " AND intExtType = " + intExtStatus, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Topic topic  = cursorToTopic(cursor);
            numCredits = numCredits + topic.getNumCredits();
            topics.add(topic);
            cursor.moveToNext();
        }
        cursor.close();
        return numCredits;
    }


    public int getFinalCreditsByYearByIntExtType(long yearID, String type) {

        int numCredits = 0;
        String intExtStatus = type;

        ArrayList<Topic> topics = new ArrayList<Topic>();
        String[] columns = { "topicID", "topicTitle", "numCredits", "intExtType", "pracFinStatus",
                "grade", "subjectID", "yearID" };
        Cursor cursor = database.query("topic_table", columns, "yearID = " + yearID + " AND intExtType = " + intExtStatus + " AND pracFinStatus = 'Final'", null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Topic topic = cursorToTopic(cursor);
            numCredits = numCredits + topic.getNumCredits();
            topics.add(topic);
            cursor.moveToNext();
        }
        cursor.close();
        return numCredits;
    }



    public String getYearDateTitle(long yearID) {
        String[] columns = { "date" };
        Cursor cursor = database.query("year_table", columns, "yearID = " + yearID, null, null, null, null);
        cursor.moveToFirst();
        year = cursorToYear(cursor);
        String date = year.getDate();
        // make sure to close the cursor
        cursor.close();
        return date;
    }


    private Subject cursorToSubject(Cursor cursor) {
        Subject subject = new Subject();
        subject.setSubjectID(cursor.getLong(0));
        subject.setSubjectName(cursor.getString(1));
        subject.setSubjectEndorseStatus(cursor.getString(2));
        subject.setSubjectLevel(cursor.getString(3));
        subject.setSubjectGoal(cursor.getString(4));
        subject.setYearID(cursor.getLong(5));
        return subject;
    }


    private Topic cursorToTopic(Cursor cursor) {
        Topic topic = new Topic();
        topic.setTopicID(cursor.getLong(0));
        topic.setTopicTitle(cursor.getString(1));
        topic.setNumCredits(cursor.getInt(2));
        topic.setIntExtType(cursor.getString(3));
        topic.setPracFinStatus(cursor.getString(4));
        topic.setGrade(cursor.getString(5));
        topic.setSubjectID(cursor.getLong(6));
        topic.setYearID(cursor.getLong(7));
        return topic;
    }


    private Year cursorToYear(Cursor cursor) {
        Year year = new Year();
        year.setDate(cursor.getString(0));
        return year;
    }

}