package com.hayleyavw.nceatracker;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;
import com.hayleyavw.nceatracker.db.TopicsDataSource;


public class SubjectProgressBarActivity extends Activity{

    private TopicsDataSource datasource;
    ListView topicList;
    long subjectID;
    long yearID;

    @Override
    public void onCreate(Bundle savedInstanceState) {


        subjectID = getIntent().getLongExtra("EXTRA_SUBJECT_ID", -1);
        yearID = getIntent().getLongExtra("EXTRA_YEAR_ID", -1);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.progress_bar_layout);

        datasource = new TopicsDataSource(this);
        datasource.open();

        populateProgressBars();

    }

    public void populateProgressBars() {

        TextView currentStatus = (TextView) findViewById(R.id.current_status);


        CheckBox includePracticeCheck = (CheckBox) findViewById(R.id.include_practice_check_box);

        if (includePracticeCheck.isChecked() == true) {

            String endorseStatus = datasource.calculateEndorsementStatus(subjectID, "Null");
            currentStatus.setText("Endorsement status: " + endorseStatus);

            //overall progress bar
            TextView subjectNameView = (TextView) findViewById(R.id.subject_name);
            subjectNameView.setText(datasource.getSubjectNameTitle(subjectID));

            int notAchievedCredits = datasource.getAllGradeCreditsBySubject(subjectID, "'Not Achieved'");
            int achievedCredits = datasource.getAllGradeCreditsBySubject(subjectID, "'Achieved'");
            int meritCredits = datasource.getAllGradeCreditsBySubject(subjectID, "'Merit'");
            int excellenceCredits = datasource.getAllGradeCreditsBySubject(subjectID, "'Excellence'");
            int totalCredits = notAchievedCredits + achievedCredits + meritCredits + excellenceCredits;

            TextView creditTotalView = (TextView) findViewById(R.id.credit_total);
            creditTotalView.setText("Total Credits: " + Integer.toString(totalCredits));

            TextView notAchievedCreditTotalView = (TextView) findViewById(R.id.not_achieved_credit_total);
            notAchievedCreditTotalView.setText(Integer.toString(notAchievedCredits) + "NA");
            notAchievedCreditTotalView.setTextColor(Color.rgb(64, 136, 207));

            TextView achievedCreditTotalView = (TextView) findViewById(R.id.achieved_credit_total);
            achievedCreditTotalView.setText(Integer.toString(achievedCredits) + "A");
            achievedCreditTotalView.setTextColor(Color.rgb(22, 116, 206));

            TextView meritCreditTotalView = (TextView) findViewById(R.id.merit_credit_total);
            meritCreditTotalView.setText(Integer.toString(meritCredits) + "M");
            meritCreditTotalView.setTextColor(Color.rgb(5, 73, 139));

            TextView excellenceCreditTotalView = (TextView) findViewById(R.id.excellence_credit_total);
            excellenceCreditTotalView.setText(Integer.toString(excellenceCredits) + "E");
            excellenceCreditTotalView.setTextColor(Color.rgb(3, 57, 108));

            //progress bar

            final float scale = this.getResources().getDisplayMetrics().density;

            double notAchievedProportion = (double) notAchievedCredits / totalCredits;
            double notAchievedDPValue = notAchievedProportion * 360;
            int notAchievedPXValue = (int) (notAchievedDPValue * scale + 0.5f);

            double achievedProportion = (double) achievedCredits / totalCredits;
            double achievedDPValue = achievedProportion * 360;
            int achievedPXValue = (int) (achievedDPValue * scale + 0.5f);

            double meritProportion = (double) meritCredits / totalCredits;
            double meritDPValue = meritProportion * 360;
            int meritPXValue = (int) (meritDPValue * scale + 0.5f);

            double excellenceProportion = (double) excellenceCredits / totalCredits;
            double excellenceDPValue = excellenceProportion * 360;
            int excellencePXValue = (int) (excellenceDPValue * scale + 0.5f);

            TextView notAchievedColourView = (TextView) findViewById(R.id.not_achieved_colour);
            notAchievedColourView.setBackgroundColor(Color.rgb(64, 136, 207));
            notAchievedColourView.setWidth(notAchievedPXValue);

            TextView achievedColourView = (TextView) findViewById(R.id.achieved_colour);
            achievedColourView.setBackgroundColor(Color.rgb(22, 116, 206));
            achievedColourView.setWidth(achievedPXValue);

            TextView meritColourView = (TextView) findViewById(R.id.merit_colour);
            meritColourView.setBackgroundColor(Color.rgb(5, 73, 139));
            meritColourView.setWidth(meritPXValue);

            TextView excellenceColourView = (TextView) findViewById(R.id.excellence_colour);
            excellenceColourView.setBackgroundColor(Color.rgb(3, 57, 108));
            excellenceColourView.setWidth(excellencePXValue);


            // int/ext progress bar

            int intCredits = datasource.getAllCreditsBySubjectByIntExtType(subjectID, "'Internal'");
            int extCredits = datasource.getAllCreditsBySubjectByIntExtType(subjectID, "'External'");

            TextView intExtView = (TextView) findViewById(R.id.credit_int_ext);
            intExtView.setText("Internal vs External");

            TextView intView = (TextView) findViewById(R.id.int_credits);
            intView.setText(Integer.toString(intCredits) + "Int");
            intView.setTextColor(Color.rgb(64, 136, 207));

            TextView extView = (TextView) findViewById(R.id.ext_credits);
            extView.setText(Integer.toString(extCredits) + "Ext");
            extView.setTextColor(Color.rgb(3, 57, 108));

            //progress bar

            final float intExtScale = this.getResources().getDisplayMetrics().density;

            double intProportion = (double) intCredits / totalCredits;
            double intDPValue = intProportion * 360;
            int intPXValue = (int) (intDPValue * intExtScale + 0.5f);

            double extProportion = (double) extCredits / totalCredits;
            double extDPValue = extProportion * 360;
            int extPXValue = (int) (extDPValue * intExtScale + 0.5f);

            TextView intColourView = (TextView) findViewById(R.id.int_colour);
            intColourView.setBackgroundColor(Color.rgb(64, 136, 207));
            intColourView.setWidth(intPXValue);

            TextView extColourView = (TextView) findViewById(R.id.ext_colour);
            extColourView.setBackgroundColor(Color.rgb(3, 57, 108));
            extColourView.setWidth(extPXValue);


        } else {

            String endorseStatus = datasource.calculateEndorsementStatus(subjectID, "'Final'");
            currentStatus.setText("Endorsement status: " + endorseStatus);

            //overall progress bar
            TextView subjectNameView = (TextView) findViewById(R.id.subject_name);
            subjectNameView.setText(datasource.getSubjectNameTitle(subjectID));

            int notAchievedCredits = datasource.getFinalGradeCreditsBySubject(subjectID, "'Not Achieved'");
            int achievedCredits = datasource.getFinalGradeCreditsBySubject(subjectID, "'Achieved'");
            int meritCredits = datasource.getFinalGradeCreditsBySubject(subjectID, "'Merit'");
            int excellenceCredits = datasource.getFinalGradeCreditsBySubject(subjectID, "'Excellence'");
            int totalCredits = notAchievedCredits + achievedCredits + meritCredits + excellenceCredits;

            TextView creditTotalView = (TextView) findViewById(R.id.credit_total);
            creditTotalView.setText("Total Credits: " + Integer.toString(totalCredits));

            TextView notAchievedCreditTotalView = (TextView) findViewById(R.id.not_achieved_credit_total);
            notAchievedCreditTotalView.setText(Integer.toString(notAchievedCredits) + "NA");
            notAchievedCreditTotalView.setTextColor(Color.rgb(64, 136, 207));

            TextView achievedCreditTotalView = (TextView) findViewById(R.id.achieved_credit_total);
            achievedCreditTotalView.setText(Integer.toString(achievedCredits) + "A");
            achievedCreditTotalView.setTextColor(Color.rgb(22, 116, 206));

            TextView meritCreditTotalView = (TextView) findViewById(R.id.merit_credit_total);
            meritCreditTotalView.setText(Integer.toString(meritCredits) + "M");
            meritCreditTotalView.setTextColor(Color.rgb(5, 73, 139));

            TextView excellenceCreditTotalView = (TextView) findViewById(R.id.excellence_credit_total);
            excellenceCreditTotalView.setText(Integer.toString(excellenceCredits) + "E");
            excellenceCreditTotalView.setTextColor(Color.rgb(3, 57, 108));

            //progress bar

            final float scale = this.getResources().getDisplayMetrics().density;

            double notAchievedProportion = (double) notAchievedCredits / totalCredits;
            double notAchievedDPValue = notAchievedProportion * 360;
            int notAchievedPXValue = (int) (notAchievedDPValue * scale + 0.5f);

            double achievedProportion = (double) achievedCredits / totalCredits;
            double achievedDPValue = achievedProportion * 360;
            int achievedPXValue = (int) (achievedDPValue * scale + 0.5f);

            double meritProportion = (double) meritCredits / totalCredits;
            double meritDPValue = meritProportion * 360;
            int meritPXValue = (int) (meritDPValue * scale + 0.5f);

            double excellenceProportion = (double) excellenceCredits / totalCredits;
            double excellenceDPValue = excellenceProportion * 360;
            int excellencePXValue = (int) (excellenceDPValue * scale + 0.5f);

            TextView notAchievedColourView = (TextView) findViewById(R.id.not_achieved_colour);
            notAchievedColourView.setBackgroundColor(Color.rgb(64, 136, 207));
            notAchievedColourView.setWidth(notAchievedPXValue);

            TextView achievedColourView = (TextView) findViewById(R.id.achieved_colour);
            achievedColourView.setBackgroundColor(Color.rgb(22, 116, 206));
            achievedColourView.setWidth(achievedPXValue);

            TextView meritColourView = (TextView) findViewById(R.id.merit_colour);
            meritColourView.setBackgroundColor(Color.rgb(5, 73, 139));
            meritColourView.setWidth(meritPXValue);

            TextView excellenceColourView = (TextView) findViewById(R.id.excellence_colour);
            excellenceColourView.setBackgroundColor(Color.rgb(3, 57, 108));
            excellenceColourView.setWidth(excellencePXValue);


            // int/ext progress bar

            int intCredits = datasource.getFinalCreditsBySubjectByIntExtType(subjectID, "'Internal'");
            int extCredits = datasource.getFinalCreditsBySubjectByIntExtType(subjectID, "'External'");

            TextView intExtView = (TextView) findViewById(R.id.credit_int_ext);
            intExtView.setText("Internal vs External");

            TextView intView = (TextView) findViewById(R.id.int_credits);
            intView.setText(Integer.toString(intCredits) + "Int");
            intView.setTextColor(Color.rgb(64, 136, 207));

            TextView extView = (TextView) findViewById(R.id.ext_credits);
            extView.setText(Integer.toString(extCredits) + "Ext");
            extView.setTextColor(Color.rgb(3, 57, 108));

            //progress bar

            final float intExtScale = this.getResources().getDisplayMetrics().density;

            double intProportion = (double) intCredits / totalCredits;
            double intDPValue = intProportion * 360;
            int intPXValue = (int) (intDPValue * intExtScale + 0.5f);

            double extProportion = (double) extCredits / totalCredits;
            double extDPValue = extProportion * 360;
            int extPXValue = (int) (extDPValue * intExtScale + 0.5f);

            TextView intColourView = (TextView) findViewById(R.id.int_colour);
            intColourView.setBackgroundColor(Color.rgb(64, 136, 207));
            intColourView.setWidth(intPXValue);

            TextView extColourView = (TextView) findViewById(R.id.ext_colour);
            extColourView.setBackgroundColor(Color.rgb(3, 57, 108));
            extColourView.setWidth(extPXValue);
        }

    }




    @Override
    protected void onResume() {
        datasource.open();
        super.onResume();
    }


    @Override
    protected void onPause() {
        datasource.close();
        super.onPause();
    }


    public void onCheckButtonClick(View v) {
        populateProgressBars();
    }

    public static Intent startActivity(Activity activity, long subjectID, long yearID) {
        Intent intent = new Intent(activity, SubjectProgressBarActivity.class);
        intent.putExtra("EXTRA_SUBJECT_ID", subjectID);
        intent.putExtra("EXTRA_YEAR_ID", yearID);
        activity.startActivity(intent);
        return intent;
    }
}
