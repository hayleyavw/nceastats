package com.hayleyavw.nceatracker;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.*;
import android.widget.AdapterView.OnItemClickListener;
import com.hayleyavw.nceatracker.db.SubjectsDataSource;
import com.hayleyavw.nceatracker.model.Subject;
import android.widget.ArrayAdapter;

import com.hayleyavw.nceatracker.ui.SubjectRowAdapter;

import java.util.ArrayList;



public class SubjectActivity extends Activity implements OnItemClickListener {

    private SubjectsDataSource datasource;
    ListView subjectList;
    long yearID;
    String level;


    @Override
    public void onCreate(Bundle savedInstanceState) {

        yearID = getIntent().getLongExtra("EXTRA_YEAR_ID", -1);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.subject_list_layout);

        datasource = new SubjectsDataSource(this);
        datasource.open();

        ArrayList<Subject> values = datasource.getSubjectsByYear(yearID);

        subjectList = (ListView) findViewById(R.id.subjectList);
        subjectList.setOnItemClickListener(this);

        SubjectRowAdapter subjectAdapter = new SubjectRowAdapter(this, values);

        subjectList.setAdapter(subjectAdapter);

        TextView yearDateView = (TextView) findViewById(R.id.year_date);
        yearDateView.setText(datasource.getYearDateTitle(yearID));


        int notAchievedCredits = datasource.getGradeCreditsByYear(yearID, "'Not Achieved'");
        int achievedCredits = datasource.getGradeCreditsByYear(yearID, "'Achieved'");
        int meritCredits = datasource.getGradeCreditsByYear(yearID, "'Merit'");
        int excellenceCredits = datasource.getGradeCreditsByYear(yearID, "'Excellence'");
        int totalCredits = notAchievedCredits + achievedCredits + meritCredits + excellenceCredits;

        TextView creditTotalView = (TextView) findViewById(R.id.year_credit_total);
        creditTotalView.setText("Total Credits: " + Integer.toString(totalCredits));

        TextView notAchievedCreditTotalView = (TextView) findViewById(R.id.year_not_achieved_credit_total);
        notAchievedCreditTotalView.setText(Integer.toString(notAchievedCredits) + "NA");
        notAchievedCreditTotalView.setTextColor(Color.rgb(64, 136, 207));

        TextView achievedCreditTotalView = (TextView) findViewById(R.id.year_achieved_credit_total);
        achievedCreditTotalView.setText(Integer.toString(achievedCredits) + "A");
        achievedCreditTotalView.setTextColor(Color.rgb(22, 116, 206));

        TextView meritCreditTotalView = (TextView) findViewById(R.id.year_merit_credit_total);
        meritCreditTotalView.setText(Integer.toString(meritCredits) + "M");
        meritCreditTotalView.setTextColor(Color.rgb(5, 73, 139));

        TextView excellenceCreditTotalView = (TextView) findViewById(R.id.year_excellence_credit_total);
        excellenceCreditTotalView.setText(Integer.toString(excellenceCredits) + "E");
        excellenceCreditTotalView.setTextColor(Color.rgb(3, 57, 108));

        //progress bar
        final float scale = this.getResources().getDisplayMetrics().density;

        double notAchievedProportion = (double) notAchievedCredits/totalCredits;
        double notAchievedDPValue = notAchievedProportion*360;
        int notAchievedPXValue = (int) (notAchievedDPValue * scale + 0.5f);

        double achievedProportion = (double) achievedCredits/totalCredits;
        double achievedDPValue = achievedProportion*360;
        int achievedPXValue = (int) (achievedDPValue * scale + 0.5f);

        double meritProportion = (double) meritCredits/totalCredits;
        double meritDPValue = meritProportion*360;
        int meritPXValue = (int) (meritDPValue * scale + 0.5f);

        double excellenceProportion = (double) excellenceCredits/totalCredits;
        double excellenceDPValue = excellenceProportion*360;
        int excellencePXValue = (int) (excellenceDPValue * scale + 0.5f);

        TextView notAchievedColourView = (TextView) findViewById(R.id.not_achieved_colour);
        notAchievedColourView.setBackgroundColor(Color.rgb(64, 136, 207));
        notAchievedColourView.setWidth(notAchievedPXValue);

        TextView achievedColourView = (TextView) findViewById(R.id.achieved_colour);
        achievedColourView.setBackgroundColor(Color.rgb(22, 116, 206));
        achievedColourView.setWidth(achievedPXValue);

        TextView meritColourView = (TextView) findViewById(R.id.merit_colour);
        meritColourView.setBackgroundColor(Color.rgb(5, 73, 139));
        meritColourView.setWidth(meritPXValue);

        TextView excellenceColourView = (TextView) findViewById(R.id.excellence_colour);
        excellenceColourView.setBackgroundColor(Color.rgb(3, 57, 108));
        excellenceColourView.setWidth(excellencePXValue);

    }


    public void onItemClick(AdapterView<?> adapter, View view, int position, long arg) {
        Subject subjectItem = (Subject) subjectList.getItemAtPosition(position);
        TopicActivity.startActivity(this, subjectItem.getSubjectID(), yearID);
    }


    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.add:
                ArrayAdapter<Subject> adapter = (ArrayAdapter<Subject>) subjectList.getAdapter();
                Subject subject = null;
                AddSubjectActivity.startActivity(this, yearID);
                adapter.notifyDataSetChanged();
                break;
            case R.id.not_achieved_colour:
                YearProgressBarActivity.startActivity(this, yearID);
                break;
            case R.id.achieved_colour:
                YearProgressBarActivity.startActivity(this, yearID);
                break;
            case R.id.merit_colour:
                YearProgressBarActivity.startActivity(this, yearID);
                break;
            case R.id.excellence_colour:
                YearProgressBarActivity.startActivity(this, yearID);
                break;
        }
    }


    @Override
    public void onBackPressed() {
        YearActivity.startActivity(this);
    }


    @Override
    protected void onResume() {
        datasource.open();
        super.onResume();
    }


    @Override
    protected void onPause() {
        datasource.close();
        super.onPause();
    }


    public static Intent startActivity(Activity activity, long yearID) {
        Intent intent = new Intent(activity, SubjectActivity.class);
        intent.putExtra("EXTRA_YEAR_ID", yearID);
        activity.startActivity(intent);
        return intent;
    }

}